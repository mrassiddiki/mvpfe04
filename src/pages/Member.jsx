import { React, useState, useEffect } from 'react'
import headerIMG from "../assets/profileIMG.jpg";
import '../components/style.css';
import login from '../components/bg_login.png';
import { Redirect, useHistory } from 'react-router-dom';
import { useParams } from "react-router";

export default function Member() {
    const removeUserSession = () => {
        localStorage.removeItem('token');
    }
    const history = useHistory();
    const handleLogout = () => {
        removeUserSession();
        history.push("/login");
    };

    let { idmember } = useParams();
    const token = localStorage.getItem('token');
    //const [data, setData] = useState({});
    const [akun, setAkun] = useState([]);
    const [member, setMember] = useState([]);
    const [mode, setMode] = useState("view");
    const [akunTrending, setAkunTrending] = useState([]);

    const handleSave = () => {
        setMode("view");
    };

    const handleEdit = () => {
        setMode("edit");
    };


    function getAkun() {
        fetch('http://kelompok4.dtstakelompok1.com/api/v1/account', {
            method: 'GET',
            headers: {
                'content-type': 'application/json',
                'Authorization': token
            },
            mode: 'cors'
        }).then(function (res) {
            res.json().then(function (json) {
                //console.log(json.data.account);
                setAkun(json.data.account);
            });
        });
    }

    function getAccountTrending() {
        fetch('http://kelompok4.dtstakelompok1.com/api/v1/accounttrending', {
            method: 'GET',
            headers: {
                'content-type': 'application/json',
                'Authorization': token
            },
            mode: 'cors'
        }).then(function (res) {
            res.json().then(function (json) {
                //console.log(json);
                setAkunTrending(json.data.account);
            });
        });
    }

    function getMember() {
        fetch('http://kelompok4.dtstakelompok1.com/api/v1/member/'+idmember, {
            method: 'GET',
            headers: {
                'content-type': 'application/json',
                'Authorization': token
            },
            mode: 'cors'
        }).then(function (res) {
            res.json().then(function (json) {
                console.log(json.data.account);
                setMember(json.data.account);
                //console.log(json.data.account);
            });
        });
    }


    useEffect(() => {
        getAccountTrending();
        getAkun();
        getMember();

    }, [])

    if (!token) {
        return <Redirect to="/login" />
    }

    const renderAccountTrending = () => {
        return akunTrending && akunTrending.map(({ id, name, contribution }) => {
            return (

                <a href={'/member/'+id} className="card-text">{name} - Point : {contribution}</a>

            )
        })
    }

    return (
        <div>
            <div class="content-bg bg-user">
                <img class="image-bg" src={login} />
            </div>

            <main className="container-fluid">
                <div className="row main-wrap ">
                    <div className="col-sm-3">
                        <div class="card w-100 my-4  text-center">
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-2">
                                        <img className="img-dashboard mt-1" src={headerIMG} />
                                    </div>
                                    <div className="col-8">
                                        <h4 class="card-title">
                                            {akun.name}
                                        </h4>
                                        <p class="card-subtitle mb-2 text-muted">
                                            {akun.role}
                                        </p>
                                    </div>
                                </div>
                                <hr />
                                <h5><a href="/dashboard"> Dashboard </a></h5>
                                <h5><a href="/profie"> Profil</a></h5>
                                <hr />
                                <h5><a href="/proyek"> Proyek Saya</a></h5>
                                <h5><a href="/group"> Grup Saya</a></h5>
                                <hr />
                                <h5><a type="button" class="text-danger" onClick={handleLogout} value="Logout"> Keluar </a></h5>
                            </div>
                        </div>

                        <div clasName=" col-sm-3 ">
                            <div class="card w-100 my-4">
                                <div className="card-body">
                                    <h5 className="card-header">Member Teraktif</h5><br />
                                    <hr />
                                    <div className="d-flex flex-column">
                                        {renderAccountTrending()}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <header className="col-sm-8 ">
                        <div className="card my-4">
                            <div className="card-body">
                                <div className="row">
                                    <div class="col-4">
                                        {mode === "view" ? (
                                            <img className="img-header" src={headerIMG} />
                                        ) : (
                                                <form>
                                                    <div class="form-group">
                                                        <label for="exampleFormControlFile1">Ganti foto profile</label>
                                                        <input type="file" class="form-control-file" id="exampleFormControlFile1" />
                                                    </div>
                                                </form>
                                            )}
                                    </div>
                                    <div class="col">
                                        <h3 class="card-title">
                                            {member.name}
                                        </h3>
                                        <p class="card-subtitle mb-2 text-muted">
                                        </p>
                                        <h5 class="card-title">
                                            {member.email}
                                        </h5>
                                        <h5 class="card-title">
                                            Hal yang disukai :
                                         </h5>
                                        <p class="card-title">
                                            Teknologi | Hiburan | Website
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <main className="">
                            <div className="card my-4">
                                <div className="card-body">
                                    <h4 className="text-center">Profile</h4>
                                    <hr />
                                    <div className="d-flex flex-row">
                                        <div className="col-sm-4">
                                            <b>
                                                <p>Nama</p>
                                            </b>
                                            {mode === "view" ? (
                                                <p id="name">{member.name}</p>
                                            ) : (
                                                    <input
                                                        type="text"
                                                        className="form-control w-50"
                                                        name="name"
                                                    />
                                                )}

                                            <b>
                                                <p>Email</p>
                                            </b>
                                            {mode === "view" ? (
                                                <p id="emailLama">{member.email}</p>
                                            ) : (
                                                    <input
                                                        type="email"
                                                        className="form-control w-50"
                                                        name="email"
                                                        id="emailInp"
                                                    />
                                                )}

                                            <b>
                                                <p>Alamat</p>
                                            </b>
                                            {mode === "view" ? (
                                                <p id="address">{member.address}</p>
                                            ) : (
                                                    <textarea
                                                        type="text"
                                                        className="form-control w-60 h-40"
                                                        name="address"
                                                        id="addressInp"
                                                    />
                                                )}


                                        </div>
                                        <div className="col-sm-4">
                                            <b>
                                                <p>No. Telepon</p>
                                            </b>
                                            {mode === "view" ? (
                                                <p id="nohp">{member.nohp}</p>
                                            ) : (
                                                    <input
                                                        type="tel"
                                                        className="form-control w-50"
                                                        name="nohp"
                                                    />
                                                )}

                                            <b>
                                                <p>Jenis Kelamin</p>
                                            </b>
                                            {mode === "view" ? (
                                                <p id="gender">{member.gender}</p>
                                            ) : (
                                                    <input
                                                        type="text"
                                                        className="form-control w-50"
                                                        name="gender"
                                                        id="genderInp"
                                                    />
                                                )}
                                            {mode === "view" ? (
                                                <p id="sandiBaru" type="password"></p>
                                            ) : (
                                                    <>
                                                        <b>
                                                            <p>Sandi Lama</p>
                                                        </b>
                                                        <input
                                                            type="password"
                                                            className="form-control w-50"
                                                            name="passwordlama"
                                                            id="sandiBaruInp"
                                                        />
                                                    </>
                                                )}
                                        </div>
                                        <div className="col-sm-4">
                                            <b>
                                                <p>Kota</p>
                                            </b>
                                            {mode === "view" ? (
                                                <p id="city">{member.city}</p>
                                            ) : (
                                                    <input
                                                        type="text"
                                                        className="form-control w-50"
                                                        name="city"
                                                    />
                                                )}

                                            <b>
                                                <p>Provinsi</p>
                                            </b>
                                            {mode === "view" ? (
                                                <p id="Province">{member.Province}</p>
                                            ) : (
                                                    <input
                                                        type="text"
                                                        className="form-control w-50 mb-1"
                                                        name="Province"
                                                        id="ProvinceInp"
                                                    />
                                                )}
                                            {mode === "view" ? (
                                                <p id="sandiBaru" type="password"></p>
                                            ) : (
                                                    <>
                                                        <b>
                                                            <p>Sandi Baru</p>
                                                        </b>
                                                        <input
                                                            type="password"
                                                            className="form-control w-50"
                                                            name="password"
                                                            id="sandiBaruInp"
                                                        />
                                                    </>
                                                )}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </main>
                    </header>





                </div>
            </main>

        </div>
    )
}
