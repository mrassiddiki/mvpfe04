import React from "react";
import { Link } from 'react-router-dom';
import { Button, Image } from "react-bootstrap";
import '../components/style.css';
import login from '../components/bg_login.png';

function Verifyemail() {
  let { token } = useParams();
    console.log(token);
    
    const[redirect, setRedirect] = useState(false);

    function verifyAkun(){
        fetch('http://kelompok4.dtstakelompok1.com/api/v1/verifiedemail/'+token,{
        method: 'GET',
        headers: {     
            'content-type':'application/json'
        },
        mode: 'cors'
    }).then(function(res){
        res.json().then(function(json){
            
            console.log(json);
            if(json.code==200){
                setRedirect(true);
            } else{
                setRedirect(false);
            }
            
        });
    });
    }

    
    useEffect(() => {
        verifyAkun();
    }, [])

  return (
    <div>
    <div class="content-bg bg-user">
        <img class="image-bg" src={login}/>
    </div>
        <div className="success-form">
        <div className="col-lg-5">
          <div className="card rounded-lg" >
            <div className="Login">
              <h3 className="text-center">Email Berhasil Diverifikasi</h3>              
              <form>
                <div class="text-center">
                  <Button className="font-weight-bold my-3" variant="outline-primary" disabled>
                    <Link to="/login">Kembali ke Login</Link>
                  </Button>
                </div>
              </form>
            </div>
          </div>  
        </div> 
       </div>
    </div>
  );
}

export default Verifyemail