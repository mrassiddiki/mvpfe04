import { useParams } from "react-router";
import { React, useState, useEffect } from 'react'
import headerIMG from "../assets/profileIMG.jpg";
import '../components/style.css';
import login from '../components/bg_login.png';
import { Redirect, useHistory, Link } from 'react-router-dom';

function Article() {
    const removeUserSession = () => {
        localStorage.removeItem('token');
    }
    const history = useHistory();
    const handleLogout = () => {
        removeUserSession();
        history.push("/login");
    };

    let { idarticle } = useParams();
    let idartc = parseInt(idarticle);
    const token = localStorage.getItem('token');
    //const [data, setData] = useState({});
    const [akun, setAkun] = useState([]);
    const [text, setText] = useState('');
    const [isiText, setIsiText] = useState({});
    const [akunTrending, setAkunTrending] = useState([]);
    const [articlesTrending, setArticlesTrending] = useState([]);
    const [name, setName] = useState('');
    const [gender, setGender] = useState('');
    const [articles, setArticles] = useState([]);
    const [comment, setComment] = useState([]);
    const [flag, setFlag] = useState(0);

    const [textComment, setTextComment] = useState('');
    const [textArticle, setTextArticle] = useState('');

    const onChangeTextComment = (e) => {
        const value = e.target.value;
        setTextComment(value);
        console.log(textComment);
    }


    function getAkun() {
        fetch('http://kelompok4.dtstakelompok1.com/api/v1/account', {
            method: 'GET',
            headers: {
                'content-type': 'application/json',
                'Authorization': token
            },
            mode: 'cors'
        }).then(function (res) {
            res.json().then(function (json) {
                //console.log(json.data.account);
                setAkun(json.data.account);
                setName(json.data.account.name);
            });
        });
    }

    function getAccountTrending() {
        fetch('http://kelompok4.dtstakelompok1.com/api/v1/accounttrending', {
            method: 'GET',
            headers: {
                'content-type': 'application/json',
                'Authorization': token
            },
            mode: 'cors'
        }).then(function (res) {
            res.json().then(function (json) {
                //console.log(json);
                setAkunTrending(json.data.account);
            });
        });
    }

    function getArticlesTrending() {
        fetch('http://kelompok4.dtstakelompok1.com/api/v1/articletrending', {
            method: 'GET',
            headers: {
                'content-type': 'application/json',
                'Authorization': token
            },
            mode: 'cors'
        }).then(function (res) {
            res.json().then(function (json) {
                //console.log(json);
                setArticlesTrending(json.data.articles);
            });
        });
    }

    function getArticles() {
        fetch('http://kelompok4.dtstakelompok1.com/api/v1/detailarticle/'+idarticle, {
            method: 'GET',
            headers: {
                'content-type': 'application/json',
                'Authorization': token
            },
            mode: 'cors'
        }).then(function (res) {
            res.json().then(function (json) {
                console.log(json.data.articles);
                //console.log(json.data['articles'].title);
                setArticles(json.data.articles);
            });
        });
    }

    function getComments() {
        fetch('http://kelompok4.dtstakelompok1.com/api/v1/getcomments/' + idarticle, {
            method: 'GET',
            headers: {
                'content-type': 'application/json',
                'Authorization': token
            },
            mode: 'cors'
        }).then(function (res) {
            res.json().then(function (json) {
                //console.log(json.data.comment);
                //console.log(json.data['articles'].title);
                setComment(json.data.comment);
                if(comment.length >0){
                    setFlag(1);
                } else {
                    setFlag(2);
                }
            });
        });
    }

    const postComment = () => {
        const data = {
            article_id: idartc,
            comment: textComment
        }

        fetch('http://kelompok4.dtstakelompok1.com/api/v1/comment/add', {
            method: 'post',
            headers: {
                'content-type': 'application/json',
                'Authorization': token
            },
            mode: 'cors',
            body: JSON.stringify(data),
        }).then(function (res) {
            console.log(res);
            getComments();
            setTextComment('');
        }).catch(err =>
            console.log(err)
        );
    }

    useEffect(() => {
        getAkun();
        getArticles();
        getComments();
        getAccountTrending();
        getArticlesTrending();

    }, [])

    if (!token) {
        return <Redirect to="/login" />
    }

    const renderBody = () => {
        return articles && articles.map(({ id, title, text_article, like, Author, description, user_id, thumbnail, timestamp, category, ArticleComment }) => {
            return (
                <div class="my-2">
                    <div class="card-body">
                        <div className="row">
                            <div className="col-2">
                                <img className="img-dashboard mt-2" src={headerIMG} />
                            </div>
                            <div className="col-10">
                                <h4 class="card-title">
                                    <b>{title}</b>
                                </h4>
                                <h6>
                                    Author : {Author}
                                </h6>
                                <p class="card-text">
                                    {text_article}</p>
                                    Category : {category}
                                <br></br>
                                <div className="d-flex ">
                                    <button className="btn btn-danger">{like} suka </button>
                                    <button className="btn btn-warning">{comment.length} komentar </button>
                                    <button className="btn btn-primary">bagikan </button>
                                </div>
                                <hr></hr>
                            </div>
                        </div>
                    </div>
                </div>
            )
        })
    }

    const renderComment = () => {
        return comment && comment.map(({ id, Name, user_id, article_id, comment }) => {
            return (
                <div>
                    <b><p>{Name}</p></b>
                    <p style={{ paddingLeft: 20 }}>{comment}</p> <hr></hr>
                </div>
            )
        })
    }

    const renderAccountTrending = () => {
        return akunTrending && akunTrending.map(({ id, name, contribution }) => {
            return (

                <a href={'/member/' + id} className="card-text">{name} - Point : {contribution}</a>

            )
        })
    }

    const renderArticlesTrending = () => {
        return articlesTrending && articlesTrending.map(({ id, title }) => {
            return (

                <a href={'/article/' + id}  className="card-text">{title}</a>

            )
        })
    }

    return (
        <>
            <div class="content-bg bg-user">
                <img class="image-bg" src={login} />
            </div>

            <main className="container-fluid">
                <div className="row main-wrap ">
                    <div className="col-sm-3">
                        <div class="card w-100 my-4  text-center">
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-2">
                                        <img className="img-dashboard mt-1" src={headerIMG} />
                                    </div>
                                    <div className="col-8">
                                        <h4 class="card-title">
                                            {akun.name}
                                        </h4>
                                        <p class="card-subtitle mb-2 text-muted">
                                            {akun.role}
                                        </p>
                                    </div>
                                </div>
                                <hr />
                                <h5><a href="/dashboard"> Beranda </a></h5>
                                <hr />
                                <h5><a href="/profile"> Profil </a></h5>
                                <hr />
                                <h5><a href="/group"> Grup </a></h5>
                                <hr />
                                <h5><a type="button" class="text-danger" onClick={handleLogout} value="Logout"> Keluar </a></h5>
                            </div>
                        </div>
                    </div>

                    <div class="card w-100 my-4 col-sm-6">
                        <div className="row my-4">
                        </div>
                        {renderBody()}
                        <hr style={{borderHeight:10}}></hr>
                        <div style={{ paddingLeft: 80 }}>
                            <h4>Komentar</h4><br></br>
                            {comment.length > 0 ? (renderComment()) : (<div><p>"Belum ada komentar"</p></div>)}
                            
                        </div>
                        <hr></hr>
                        <div>
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Berikan Komentar Anda</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Berikan Komentar..." value={textComment} onChange={onChangeTextComment}></textarea>
                                <button type="submit" class="btn btn-success mt-2" onClick={postComment} >Post</button>
                            </div>
                        </div>
                    </div>

                    <div className="col-sm-3">
                        <div class="card border-success w-100 my-4">
                            <div className="card-body">
                                <h5 className="card-header">Article Trending</h5>
                                <div className="d-flex flex-column">
                                    {renderArticlesTrending()}
                                </div>
                            </div>
                            <hr />
                            <div className="card-body">
                                <h5 className="card-header">Member Terbaik</h5>
                                <div className="d-flex flex-column">
                                    {renderAccountTrending()}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </main>
            <div>
            </div>
        </>
    );
}

export default Article;
