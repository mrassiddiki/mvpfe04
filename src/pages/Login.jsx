import {useState, Fragment} from 'react';
import { Image } from "react-bootstrap";
import { Redirect } from 'react-router-dom';
import '../components/style.css';
import login from '../components/bg_login.png';

function Login() {
    const[email, setEmail] = useState('');
    const[password, setPassword] = useState('');
    const[redirect, setRedirect] = useState(false);
    const[flag, setFlag] = useState(0);

    const onChangeEmail = (e) => {
        const value = e.target.value;
        setEmail(value);
    }

    const onChangePassword = (e) => {
        const value = e.target.value;
        setPassword(value);
    }

    const LoginAkun = () => {
        const data = {
            email: email,
            password: password
        }
        
        fetch('http://kelompok4.dtstakelompok1.com/api/v1/login',{
            method:'post',
            headers: {     
                'content-type':'application/json',
            },
            mode: 'cors',
            body: JSON.stringify(data),
        }).then(function(res){
            var token = res.headers.get('Authorization');
            console.log(token);
            if(token != null){
                localStorage.setItem('token', token);
                setRedirect(true);
                setFlag(1);
            } else {
                setRedirect(false);
                setFlag(2);
            }
        })
    }

    return(
        <Fragment>
            {
                redirect && (
                    <Redirect to="/profile" />
                )
            }
            <div>
                <div class="content-bg bg-user">
                    <img class="image-bg" src={login}/>
                </div>

                <div className="login-form">
                <div className="col-lg-5">
                    <div className="card rounded-lg">
                    <div className="Login">
                        <div className="col-md-12">
                        <h3 className="text-center">Login</h3>
                        </div>
                        <div className="col-md-12">
                            <div className="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="email" className="form-control" name="email" aria-describedby="emailHelp" placeholder="Email" value={email} onChange={onChangeEmail} />
                            </div>
                            <div className="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" className="form-control" name="password" placeholder="Password" value={password} onChange={onChangePassword} />
                            </div>
                            {flag === 2 && 
                            <div>
                                <label for="cek password" style={{paddingLeft: "100px", color: "red"}}>Email / Password Salah !</label>
                            </div>
                            }
                            <div className="form-group form-check">
                            <input type="checkbox" className="form-check-input" id="exampleCheck1" />
                            <label className="form-check-label" for="exampleCheck1">Ingat saya</label>
                            </div>
                            <button type="submit" className="btn btn-primary btn-block" onClick={LoginAkun}>Log In</button>
                            <p className="text-center">Belum memiliki akun? <a href="/Register"> Buat akun </a></p>
                            <div className="line">
                            <h6 className="text-center"><span>atau masuk dengan</span></h6>
                            </div>
                                <a href="http://kelompok4.dtstakelompok1.com/api/v1/googlelogin">
                                <button className="btn btn-outline-danger btn-block icon-btn">
                                <i class="fa fa-google-plus"></i> Google</button>
                            </a>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
                    
                
            </div>
        </Fragment>
    )    
}

export default Login