import React, { useEffect, useState } from 'react';
import headerIMG from "../assets/profileIMG.jpg";
import login from '../components/bg_login.png';
import '../components/admin.css';
import { Redirect, useHistory } from 'react-router-dom';

export default function DashboardAdmin() {
  const removeUserSession = () => {
    localStorage.removeItem('token');
}
  const history = useHistory();
  const handleLogout = () => {
    removeUserSession();
    history.push("/loginAdmin");
  };

  const token = localStorage.getItem('token');
  const [akun, setAkun] = useState([]);

  function getAkun() {
    fetch('http://kelompok4.dtstakelompok1.com/api/v1/allaccount', {
      method: 'GET',
      headers: {
        'content-type': 'application/json',
        'Authorization': token
      },
      mode: 'cors'
    }).then(function (res) {
      res.json().then(function (json) {
        //console.log(json.data.account);
          setAkun(json.data.account);
          console.log(akun);
        

      });
    });
  }

  useEffect(() => {
    getAkun();
  }, [])

  const renderHeader = () => {
    let headerElement = ['Nama', 'Email', 'Status Verifikasi', 'aksi']

    return headerElement.map((key, index) => {
      return <th scop="col" key={index}>{key.toUpperCase()}</th>
    })
  }

  const renderBody = () => {
    return akun && akun.map(({id, name, email, email_verified }) => {
      return (
        <tr key={id}>
          <td>{name}</td>
          <td>{email}</td>
          <td>{email_verified ? ("Sudah"):("Belum")}</td>
          <td>
            {email_verified ? (<button class='btn btn-danger' onClick={() => removeUser(id)}>Delete</button>) : (<div><button class='btn btn-danger' onClick={() => removeUser(id)}>Delete</button> <button class='btn btn-info' onClick={() => approveUser(id)}>Terima</button></div>)}
          </td>
        </tr>
      )
    })
  }

  const removeUser = (id) => {
    fetch(`http://kelompok4.dtstakelompok1.com/api/v1/deleteaccount/${id}`, {
      method: 'PUT',
      headers: {
        'Authorization': token
      },
      mode: 'cors'
    }).then(function (res) {
      getAkun();
    });
  }

  const approveUser = (id) => {
    fetch(`http://kelompok4.dtstakelompok1.com/api/v1/verifyaccount/${id}`, {
      method: 'PUT',
      headers: {
        'Authorization': token
      },
      mode: 'cors'
    }).then(function (res) {
      getAkun();
    }).catch(err =>
      console.log(err)
    );
  }


  if(!token){
    return <Redirect to="/loginAdmin" />
 }
  
  return (
    <div class="admin">
	<div class="content-bg bg-admin">
      <img class="image-bg" src={login}/>
    </div>

      <main className="container-fluid">
        <div className="row main-wrap ">
          <div className="col-sm-3">
            <div class="card w-100 my-4  text-center">
              <div className="card-body">
                <div className="row">
                  <div className="col-2">
                    <img className="img-dashboard mt-1" src={headerIMG} />
                  </div>
                  <div className="col-8 mt-4">
                    <h3 class="card-title">
                      Admin
                    </h3>
                  </div>
                </div>
                <hr />
                <h5><a href="/dashboard"> Dashboard Admin </a></h5>
                <h5><a href="/profieAdmin"> Profil Admin</a></h5>
                <h5><a href="/"> Data Member</a></h5>
                <h5><a href="/"> Data Grup</a></h5>
                <hr />
                <h5><a type="button" class="text-danger" onClick={handleLogout} value="Logout"> Keluar </a></h5>
              </div>
            </div>

            <div clasName=" col-sm-3 ">
              <div class="card w-100 my-4">
                <div className="card-body">
                  <h5 className="card-title">Update Member</h5>
                  <h6 className="card-subtitle mb-2 text-muted">Jumlah (80)</h6>
                  <hr />
                  <div className="d-flex flex-column">
                    <p className="card-text">Wayan membagikan postingan</p>
                    <p className="card-text">Ucup bergabung dengan grup Bangking</p>
                    <p className="card-text">Udin bergabung dengan CoCreate</p>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="col-sm-8 card w-100 my-4">
            <div className="row-1">
              <div className="my-4">
                <h4 class="text-center">Dashboard Admin</h4>
              </div>
              <hr />
              <table id="akun" class= "table table-striped">
                <thead>
                  <tr>{renderHeader()}</tr>
                </thead>
                <tbody>
                  {renderBody()}
                </tbody>
              </table>
            </div>
          </div>



        </div>
      </main>

    </div>
  )
}
